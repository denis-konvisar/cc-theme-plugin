<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

global $product;

// Ensure visibility
if ( empty( $product ) || ! $product->is_visible() ) {
    return;
}
?>
<?php // post_class(); ?>
<?php
/**
 * woocommerce_before_shop_loop_item hook.
 *
 * @hooked woocommerce_template_loop_product_link_open - 10
 */
//	do_action( 'woocommerce_before_shop_loop_item' );

/**
 * woocommerce_before_shop_loop_item_title hook.
 *
 * @hooked woocommerce_show_product_loop_sale_flash - 10
 * @hooked woocommerce_template_loop_product_thumbnail - 10
 */
//do_action( 'woocommerce_before_shop_loop_item_title' );

/**
 * woocommerce_shop_loop_item_title hook.
 *
 * @hooked woocommerce_template_loop_product_title - 10
 */
//do_action( 'woocommerce_shop_loop_item_title' );

/**
 * woocommerce_after_shop_loop_item_title hook.
 *
 * @hooked woocommerce_template_loop_rating - 5
 * @hooked woocommerce_template_loop_price - 10
 */
//	do_action( 'woocommerce_after_shop_loop_item_title' );

/**
 * woocommerce_after_shop_loop_item hook.
 *
 * @hooked woocommerce_template_loop_product_link_close - 5
 * @hooked woocommerce_template_loop_add_to_cart - 10
 */
//do_action( 'woocommerce_after_shop_loop_item' );

?>

<?php
$imgSwatch = get_field('swatch_image', $post->ID);
$imgRoom = get_field('featured_image');
?>
<?php switch (CATEGORY_TYPE): ?>
<?php case "swatch": ?>
        <?php
        if (!empty($imgSwatch['url'])) {
            $image = $imgSwatch['url'];
        }
        ?>
        <?php break; ?>
    <?php case "room": ?>
        <?php
        $image = $imgRoom;
        ?>
        <?php break; ?>
    <?php endswitch ?>
<?php
$currency = get_woocommerce_currency_symbol();
$on_sale = false;
$on_sale_without_discount =  product_on_sale_without_discount($product->get_id());

if($product->is_on_sale()) {
    $on_sale = true;
}
$special_offer = get_field('special_offers', $product->id);
?>
    <div class="product-card <?= CATEGORY_TYPE ?> p-<?= $product->get_price() ?>">
        <div class="product-card-inner">
            <a href="<?= get_the_permalink(); ?>" class="product-card-image" data-swatch="background-image: url(<?= $imgSwatch ?>)" data-room="background-image: url(<?= $imgRoom ?>)" style="background-image: url(<?php if(!(empty($image))) { echo $image; } else { echo $imgRoom;} ?>)" >
                <?php if (!empty($imgRoom)) : ?>
                    <div class="product-card-image-hover" style="background-image: url(<?= $imgRoom ?>)"></div>
                <?php endif; ?>
                <span>Explore more</span>
                <?php if($on_sale): ?>
                    <strong class="product-card-image__sale">Sale</strong>
                <?php endif; ?>
            </a>
            <div class="product-card-footer">
                <h5 class="product-card__title"><?php the_title() ?></h5>
                <div class="product-card-price">
                    <?php if($on_sale): ?>
                        <div class="product-card-price__value"><span class="-old"><?= $currency.$product->get_regular_price()?></span><span class="-new"><?= $currency.$product->get_sale_price()?></span></div>
                    <?php else : ?>
                        <div class="product-card-price__value"><span><?= $currency.$product->get_regular_price()?></span></div>
                    <?php endif; ?>
                    <div class="product-card-price__unit">*per sqm</div>
                </div>
            </div>
            <div class="product-card-extend">
                <?php if($on_sale_without_discount): ?>
                    <?php if(!empty($special_offer)): ?>
                        <div class="product-card-special">
                            <div class="product-card-special__item"><?= $special_offer ?></div>
                        </div>
                    <?php endif;?>
                <?php endif;?>
            </div>
        </div>
    </div>

<?php
/*
  *
  *
    <div class="product-card js-show-photo-parent <?= CATEGORY_TYPE ?>">
        <div class="product-card-info">
            <!--<div class="product-card__subtitle"><?= get_the_terms($post->ID, 'product_brand')[0]->name ?></div>-->
            <h5 class="product-card__title"><?php the_title() ?></h5>
            <div class="product-card-color">
                <div class="product-card-color__title">Colour Range</div>
                <ul class="product-card-color__list" style="margin-left: 0">

                    <?php
                    $colors = get_the_terms( $product->id, 'pa_color' );
                    if (!empty($colors))
                        foreach ($colors as $color){
                            $color_image = get_term_meta( $color->term_id, 'cpm_color_thumbnail', true ); ?>
                            <li class="product-card-color__item js-show-photo-trigger">
                                <div style="-webkit-mask-image: url('<?= get_template_directory_uri() ?>/static/public/images/tooltip-color.svg');mask-image: url('<?= get_template_directory_uri() ?>/static/public/images/tooltip-color.svg'); background-image:url('<?= $color_image ?>');" class="product-card-color__tooltip">
                                    <div class="product-card-color__tooltip-name"><?= $color->name ?></div>
                                </div>
                                <div class="product-card-color__img"><img style="min-width: 30px; min-height: 30px;" src="<?= $color_image ?>" alt="<?= $color->name ?>"/></div>
                            </li>
                        <?php }
                    ?>
                </ul>
            </div>
            <p class="product-card__description"><?php $text = get_the_excerpt(); $text = strWordCut($text, 120, get_the_permalink()); echo $text; ?></p>
            <ul class="product-card-features" style="margin-left: 0">
                <?php
                //  print_r( get_terms());
                $features = get_the_terms($post->ID, 'product_feature');
                if (!empty($features))
                    foreach ($features as $feature){ ?>
                        <li class="product-card-features__item"><?= $feature->name ?></li>
                    <?php }
                ?>
            </ul><a href="<?php the_permalink(); ?>" class="product-card__link button">see the range</a>
        </div>
        <div class="product-card-photo"><img src="<?= get_field('featured_image') ?>" alt="product image"/>
            <div class="product-card-example">
                <div style="background-image:url('<?= get_template_directory_uri() ?>/static/public/images/product-tool/swatch.jpg');" class="product-card-example__item js-show-photo-target">
                    <div class="product-card-example__item-name">Clare 1</div>
                </div>
                <div style="background-image:url('images/product-tool/swatch.jpg');" class="product-card-example__item js-show-photo-target">
                    <div class="product-card-example__item-name">Clare 2</div>
                </div>
                <div style="background-image:url('images/product-tool/swatch.jpg');" class="product-card-example__item js-show-photo-target">
                    <div class="product-card-example__item-name">Clare 3</div>
                </div>
                <div style="background-image:url('images/product-tool/swatch.jpg');" class="product-card-example__item js-show-photo-target">
                    <div class="product-card-example__item-name">Clare 4</div>
                </div>
                <div style="background-image:url('images/product-tool/swatch.jpg');" class="product-card-example__item js-show-photo-target">
                    <div class="product-card-example__item-name">Clare 5</div>
                </div>
                <div style="background-image:url('images/product-tool/swatch.jpg');" class="product-card-example__item js-show-photo-target">
                    <div class="product-card-example__item-name">Clare 6</div>
                </div>
            </div>
        </div>
    </div>
  *
  */

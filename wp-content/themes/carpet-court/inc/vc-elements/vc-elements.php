<?php
/**
 * Created by PhpStorm.
 * User: Rubal
 * Date: 2/11/16
 * Time: 12:48 PM
 */
require get_template_directory() . '/inc/vc-elements/elements/page-link.php';
require get_template_directory() . '/inc/vc-elements/elements/product-cat-gallery.php';
require get_template_directory() . '/inc/vc-elements/elements/gallery.php';
require get_template_directory() . '/inc/vc-elements/elements/image-gallery.php';
require get_template_directory() . '/inc/vc-elements/elements/image-gallery-idea-cat.php';
require get_template_directory() . '/inc/vc-elements/elements/tips.php';
require get_template_directory() . '/inc/vc-elements/elements/troubleshooting.php';
require get_template_directory() . '/inc/vc-elements/elements/img-vid-gallery.php';
require get_template_directory() . '/inc/vc-elements/elements/call-to-action.php';
require get_template_directory() . '/inc/vc-elements/elements/vc-diagnostics-gallery.php';
require get_template_directory() . '/inc/vc-elements/elements/cc-pdf-upload.php';

<?php
/**
 * Template Name: Template Store Profile
 * * Description: Default Page for Store Profile
 *
 * @package Carpet_Court
 */
get_header(); ?>

	<div id="primary" class="content-area store-profile-wrap">
		<main id="main" role="main">

			<!-- SLIDER-WRAP STARTS -->
			<section class="slider-wrap">
				<div class="bxslider">
					<div class="slides">
						<img src="http://staging.carpetcourt.nz/wp-content/uploads/2016/02/slider-1.jpg" alt="">
						<div class="caption">
							<div class="vert-middle">
								<div class="wrap">
									<h1 class="title">Moorhouse Ave</h1>
									<span class="desc">Something unique to this specific store</span>
								</div>
							</div>
						</div>
					</div>

					<div class="slides">
						<img src="http://staging.carpetcourt.nz/wp-content/uploads/2016/02/slider-1.jpg" alt="">
						<div class="caption">
							<div class="vert-middle">
								<div class="wrap">
									<h1 class="title">Moorhouse Ave</h1>
									<span class="desc">Something unique to this specific store</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- SLIDER-WRAP ENDS -->

            <!-- PAGE-SCROLL-NAV STARTS -->
			<div class="page-scroll-nav container wow fadeInUp">
				<ul>
					<li>
						<a href="#welcome">Welcome</a>
					</li>
					<li>
						<a href="#welcome">Testimonials</a>
					</li>
					<li>
						<a href="#about">About</a>
					</li>
					<li>
						<a href="#gallery">Gallery</a>
					</li>
					<li>
						<a href="#team">Meet the team</a>
					</li>
					<li>
						<a href="#contact">Contact & Location</a>
					</li>
				</ul>
			</div>
		    <!-- PAGE-SCROLL-NAV ENDS -->

			<!-- WELCOME STARTS -->
			<section id="welcome">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_100 vc_sep_border_width_3 vc_sep_pos_align_center vc_sep_color_grey wow fadeInUp vc_separator-has-text animated" style="visibility: visible; animation-name: fadeInUp;">
								<span class="vc_sep_holder vc_sep_holder_l">
									<span class="vc_sep_line"></span>
								</span>
								<h4>Welcome</h4>
								<span class="vc_sep_holder vc_sep_holder_r">
									<span class="vc_sep_line"></span>
								</span>
							</div>
						</div>

						<div class="col-md-12  wow fadeInUp">
							<div class="intro-text">
								<p>We'd love to came and visit us at our friendly Mpprhouse Ave store. You can find easy aprking between xx and xx streets. You'll find an extensive range of latest looks in flooring - from carpet to tiles, to timber and vinyl.</p>
								<p>If you're looking for design inpiration, check-out our large in-store flooring displays ( which have been laid as they would in a home ) and have been designed to help you visualise how it could look in your space.</p>
								<img src="http://staging.carpetcourt.nz/wp-content/uploads/2016/02/welcome.jpg" alt="welcome">
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- WELCOME ENDS -->


			<!-- TESTIMONIALS STARTS -->
			<section id="testimonials">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_100 vc_sep_border_width_3 vc_sep_pos_align_center vc_sep_color_grey wow fadeInUp vc_separator-has-text animated" style="visibility: visible; animation-name: fadeInUp;">
								<span class="vc_sep_holder vc_sep_holder_l">
									<span class="vc_sep_line"></span>
								</span>
								<h4>Testimonials</h4>
								<span class="vc_sep_holder vc_sep_holder_r">
									<span class="vc_sep_line"></span>
								</span>
							</div>
						</div>

						<div class="col-md-12  wow fadeInUp">
							<div class="bxslider">
								<div class="slides">
									<span class="quote">
										We'd love to came and visit us at our friendly Mpprhouse Ave store. You can find easy aprking between xx and xx streets. You'll find an extensive range of latest looks in flooring - from carpet to tiles, to timber and vinyl.
									</span>
									<span class="user-img">
										<img src="<?php echo get_template_directory_uri();?>/assets/images/woman.png" alt="woman">
									</span>
									<span class="user-detail">Jane Doe</span>
								</div>

								<div class="slides">
									<span class="quote">
										We'd love to came and visit us at our friendly Mpprhouse Ave store. You can find easy aprking between xx and xx streets. You'll find an extensive range of latest looks in flooring - from carpet to tiles, to timber and vinyl.
									</span>
									<span class="user-img">
										<img src="<?php echo get_template_directory_uri();?>/assets/images/man.png" alt="man">
									</span>
									<span class="user-detail">Jonie Walker</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- TESTIMONIALS ENDS -->


			<!-- ABOUT STARTS -->
			<section id="about">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_100 vc_sep_border_width_3 vc_sep_pos_align_center vc_sep_color_grey wow fadeInUp vc_separator-has-text animated" style="visibility: visible; animation-name: fadeInUp;">
								<span class="vc_sep_holder vc_sep_holder_l">
									<span class="vc_sep_line"></span>
								</span>
								<h4>About Us</h4>
								<span class="vc_sep_holder vc_sep_holder_r">
									<span class="vc_sep_line"></span>
								</span>
							</div>
						</div>

						<div class="col-md-12  wow fadeInUp">
							<div class="intro-text">
								<p>We'd love to came and visit us at our friendly Mpprhouse Ave store. You can find easy aprking between xx and xx streets. You'll find an extensive range of latest looks in flooring - from carpet to tiles, to timber and vinyl.</p>
								<p>If you're looking for design inpiration, check-out our large in-store flooring displays ( which have been laid as they would in a home ) and have been designed to help you visualise how it could look in your space.</p>
							</div>
						</div>

						<div class="col-md-6  wow fadeInUp">
							<div class="intro-text">
								<h3>History</h3>
								<p>We'd love to came and visit us at our friendly Mpprhouse Ave store. You can find easy aprking between xx and xx streets. You'll find an extensive range of latest looks in flooring - from carpet to tiles, to timber and vinyl.</p>
								<p>If you're looking for design inpiration, check-out our large in-store flooring displays ( which have been laid as they would in a home ) and have been designed to help you visualise how it could look in your space.</p>

							</div>
						</div>

						<div class="col-md-6  wow fadeInUp">
							<div class="intro-text">
								<h3>Personality</h3>
								<p>We'd love to came and visit us at our friendly Mpprhouse Ave store. You can find easy aprking between xx and xx streets. You'll find an extensive range of latest looks in flooring - from carpet to tiles, to timber and vinyl.</p>
								<p>If you're looking for design inpiration, check-out our large in-store flooring displays ( which have been laid as they would in a home ) and have been designed to help you visualise how it could look in your space.</p>

							</div>
						</div>

						<div class="space" style="height:50px;"></div>

						<div class="col-md-12  wow fadeInUp">
							<div class="vc_tta-container" data-vc-action="collapse">
								<div class="vc_general vc_tta vc_tta-tabs vc_tta-color-grey vc_tta-style-classic vc_tta-shape-rounded vc_tta-spacing-1 vc_tta-tabs-position-top vc_tta-controls-align-left">
									<div class="vc_tta-tabs-container">
										<ul class="vc_tta-tabs-list">
											<li class="vc_tta-tab vc_active" data-vc-tab=""><a href="#1461748348151-df002466-1d6f" data-vc-tabs="" data-vc-container=".vc_tta"><span class="vc_tta-title-text">Tab 1</span></a></li>
											<li class="vc_tta-tab" data-vc-tab=""><a href="#1461748348311-7e5f2b6f-47a8" data-vc-tabs="" data-vc-container=".vc_tta"><span class="vc_tta-title-text">Tab 2</span></a></li>
										</ul>
									</div>

									<div class="vc_tta-panels-container">
										<div class="vc_tta-panels">
											<div class="vc_tta-panel vc_active" id="1461748348151-df002466-1d6f" data-vc-content=".vc_tta-panel-body">
												<div class="vc_tta-panel-heading"><h4 class="vc_tta-panel-title"><a href="#1461748348151-df002466-1d6f" data-vc-accordion="" data-vc-container=".vc_tta-container"><span class="vc_tta-title-text">Tab 1</span></a></h4>
												</div>
												<div class="vc_tta-panel-body" style="">
													<div class="wpb_text_column wpb_content_element ">
														<div class="wpb_wrapper">
															<p>We'd love to came and visit us at our friendly Mpprhouse Ave store. You can find easy aprking between xx and xx streets. You'll find an extensive range of latest looks in flooring - from carpet to tiles, to timber and vinyl.</p>
															<p>If you're looking for design inpiration, check-out our large in-store flooring displays ( which have been laid as they would in a home ) and have been designed to help you visualise how it could look in your space.</p>
														</div>
													</div>
												</div>
											</div>

											<div class="vc_tta-panel" id="1461748348311-7e5f2b6f-47a8" data-vc-content=".vc_tta-panel-body">
												<div class="vc_tta-panel-heading"><h4 class="vc_tta-panel-title"><a href="#1461748348311-7e5f2b6f-47a8" data-vc-accordion="" data-vc-container=".vc_tta-container"><span class="vc_tta-title-text">Tab 2</span></a></h4>
												</div>
												<div class="vc_tta-panel-body" style="">
													<div class="wpb_text_column wpb_content_element ">
														<div class="wpb_wrapper">
															<p>We'd love to came and visit us at our friendly Mpprhouse Ave store. You can find easy aprking between xx and xx streets. You'll find an extensive range of latest looks in flooring - from carpet to tiles, to timber and vinyl.</p>
															<p>If you're looking for design inpiration, check-out our large in-store flooring displays ( which have been laid as they would in a home ) and have been designed to help you visualise how it could look in your space.</p>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</section>
			<!-- ABOUT ENDS -->


			<!-- GALLERY STARTS -->
			<section id="gallery">
				<div class="container">
					<div class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_100 vc_sep_border_width_3 vc_sep_pos_align_center vc_sep_color_grey wow fadeInUp vc_separator-has-text animated" style="visibility: visible; animation-name: fadeInUp;">
						<span class="vc_sep_holder vc_sep_holder_l">
							<span class="vc_sep_line"></span>
						</span>
						<h4>Gallery</h4>
						<span class="vc_sep_holder vc_sep_holder_r">
							<span class="vc_sep_line"></span>
						</span>
					</div>



					<div id="isotope-list" style="position: relative; height: 600px;" class="wow fadeInUp cpm-isotope-list">
		                <div class="vc_row wpb_row vc_row-fluid"><div class="wow fadeInUp wpb_column vc_column_container vc_col-sm-12 animated" style="visibility: visible; animation-name: fadeInUp;"><div class="vc_column-inner "><div class="wpb_wrapper">        <div class="living  isotope-item isotope-1xh isotope-2xw   cc-gallery-zoom " style="position: absolute; left: 0px; top: 0px;">
		            <a href="http://staging.carpetcourt.nz/wp-content/uploads/2016/03/Pegasus_Fleur_High_1-LR.jpg" rel="prettyPhoto">                                    <img width="600" height="431" src="http://staging.carpetcourt.nz/wp-content/uploads/2016/03/Pegasus_Fleur_High_1-LR-600x431.jpg" class="attachment-cc_gal_image size-cc_gal_image wp-post-image" alt="Pegasus_Fleur_High_1 LR" srcset="http://staging.carpetcourt.nz/wp-content/uploads/2016/03/Pegasus_Fleur_High_1-LR-600x431.jpg 600w, http://staging.carpetcourt.nz/wp-content/uploads/2016/03/Pegasus_Fleur_High_1-LR-112x80.jpg 112w, http://staging.carpetcourt.nz/wp-content/uploads/2016/03/Pegasus_Fleur_High_1-LR-110x80.jpg 110w" sizes="(max-width: 600px) 100vw, 600px">                                                <div class="isotop-content" data-backgroundcolor-hover="">
		                <div class="isocontent-wrap isocontent-wrap-default">
		                    <div class="isocontent-span">
		                                                    <span class="idea-title">
		                                <i class="isotope-seperator seperator-top"></i>
		                                    Demo                                <i class="isotope-seperator seperator-bottom"></i>
		                            </span>
		                                                                            <span class="idea-excerpt"></span>
		                                            </div>
		                </div>
		                <div class="idea-hover-overlay">
		                                    </div>
		                </div>
		            </a>        </div>
		            <div class="living  isotope-item isotope-1xh isotope-1xw isotope-background  " style="position: absolute; left: 380px; top: 0px; background-color: rgb(232, 129, 11);">
		                                                            <div class="isotop-content" data-backgroundcolor-hover="">
		                <div class="isocontent-wrap isocontent-wrap-default">
		                    <div class="isocontent-span">
		                                                    <span class="idea-title">
		                                <i class="isotope-seperator seperator-top"></i>
		                                    Find a designer near you                                <i class="isotope-seperator seperator-bottom"></i>
		                            </span>
		                                                                    </div>
		                </div>
		                <div class="idea-hover-overlay">
		                                    </div>
		                </div>
		                    </div>
		            <div class="living  isotope-item isotope-1xh isotope-1xw  grayscale " style="position: absolute; left: 570px; top: 0px;">
		            <a href="http://staging.carpetcourt.nz/popup-page-demo/?iframe=true&amp;width=100%&amp;height=100%" rel="prettyPhoto">                                    <img width="600" height="600" src="http://staging.carpetcourt.nz/wp-content/uploads/2016/03/Dining-001-banner-LR-600x600.jpg" class="attachment-cc_gal_image size-cc_gal_image wp-post-image" alt="Dining [001] banner LR" srcset="http://staging.carpetcourt.nz/wp-content/uploads/2016/03/Dining-001-banner-LR-600x600.jpg 600w, http://staging.carpetcourt.nz/wp-content/uploads/2016/03/Dining-001-banner-LR-150x150.jpg 150w, http://staging.carpetcourt.nz/wp-content/uploads/2016/03/Dining-001-banner-LR-300x300.jpg 300w" sizes="(max-width: 600px) 100vw, 600px">                                                <div class="isotop-content" data-backgroundcolor-hover="">
		                <div class="isocontent-wrap isocontent-wrap-default">
		                    <div class="isocontent-span">
		                                                                    </div>
		                </div>
		                <div class="idea-hover-overlay">
		                                    </div>
		                </div>
		            </a>        </div>
		            <div class="kitchens  isotope-item isotope-1xh isotope-2xw  isotope-background  isotope-hover-only" style="position: absolute; left: 760px; top: 0px; background-color: rgb(175, 239, 35);">
		            <a href="/tests-vc/" target="" title="">                                <div class="default-overlay"></div>                <div class="isotop-content" data-backgroundcolor-hover="#25e1e8">
		                <div class="isocontent-wrap isocontent-wrap-default">
		                    <div class="isocontent-span">
		                                                    <span class="idea-title">
		                                <i class="isotope-seperator seperator-top"></i>
		                                    Planning &amp; Budget                                <i class="isotope-seperator seperator-bottom"></i>
		                            </span>
		                                                                            <span class="idea-excerpt">"Get it done without grey hairs  or breaking the box"</span>
		                                            </div>
		                </div>
		                <div class="idea-hover-overlay">
		                    <div class="isocontent-wrap" style="color: #ffffff"> <div class="isocontent-span"><span class="idea-title"><i class="isotope-seperator seperator-top" style="background-color: "></i>Planning &amp; Budget<i class="isotope-seperator seperator-bottom" style="background-color: "></i></span><span class="hover-subtext clearfix">``Get it done without grey hairs or breaking the box``</span></div></div>                </div>
		                </div>
		            </a>        </div>
		            <div class="kitchens  isotope-item isotope-1xh isotope-1xw  cc-gallery-zoom isotope-hover-only" style="position: absolute; left: 0px; top: 200px;">
		                                                <img width="600" height="600" src="http://staging.carpetcourt.nz/wp-content/uploads/2016/03/Hall-Way-003-banner-V2-LR-600x600.jpg" class="attachment-cc_gal_image size-cc_gal_image wp-post-image" alt="Hall Way [003] banner V2 LR" srcset="http://staging.carpetcourt.nz/wp-content/uploads/2016/03/Hall-Way-003-banner-V2-LR-600x600.jpg 600w, http://staging.carpetcourt.nz/wp-content/uploads/2016/03/Hall-Way-003-banner-V2-LR-150x150.jpg 150w, http://staging.carpetcourt.nz/wp-content/uploads/2016/03/Hall-Way-003-banner-V2-LR-300x300.jpg 300w" sizes="(max-width: 600px) 100vw, 600px">                                <div class="default-overlay"></div>                <div class="isotop-content" data-backgroundcolor-hover="rgba(36,226,220,0.83)">
		                <div class="isocontent-wrap isocontent-wrap-default">
		                    <div class="isocontent-span">
		                                                                    </div>
		                </div>
		                <div class="idea-hover-overlay">
		                    <div class="isocontent-wrap" style="color: #ffffff"> <div class="isocontent-span"><span class="idea-title"><i class="isotope-seperator seperator-top" style="background-color: #ffffff"></i>Title<i class="isotope-seperator seperator-bottom" style="background-color: #ffffff"></i></span><span class="hover-subtext clearfix">Sub Title</span><button class="cc-btn" style="color: #ffffff; background-color: #98ed49">Button</button></div></div>                </div>
		                </div>
		                    </div>
		            <div class="bathrooms  isotope-item isotope-2xh isotope-1xw  isotope-background  " style="position: absolute; left: 190px; top: 200px; background-color: rgb(237, 54, 54);">
		                                                            <div class="isotop-content" data-backgroundcolor-hover="">
		                <div class="isocontent-wrap isocontent-wrap-default">
		                    <div class="isocontent-span">
		                                                    <span class="idea-title">
		                                <i class="isotope-seperator seperator-top"></i>
		                                    What's on trend?                                <i class="isotope-seperator seperator-bottom"></i>
		                            </span>
		                                                                            <span class="idea-excerpt">New looks, ideas and tips</span>
		                                            </div>
		                </div>
		                <div class="idea-hover-overlay">
		                                    </div>
		                </div>
		                    </div>
		            <div class="bathrooms  isotope-item isotope-2xh isotope-2xw   grayscale " style="position: absolute; left: 380px; top: 200px;">
		            <a href="http://staging.carpetcourt.nz/designer-troubleshooting/designer-q-a/" target=" _blank" title="">                                    <img width="600" height="600" src="http://staging.carpetcourt.nz/wp-content/uploads/2016/03/Luxaflex-Countrywoods-Shutters-9-LR-600x600.jpg" class="attachment-cc_gal_image size-cc_gal_image wp-post-image" alt="Luxaflex Countrywoods Shutters  (9) LR" srcset="http://staging.carpetcourt.nz/wp-content/uploads/2016/03/Luxaflex-Countrywoods-Shutters-9-LR-600x600.jpg 600w, http://staging.carpetcourt.nz/wp-content/uploads/2016/03/Luxaflex-Countrywoods-Shutters-9-LR-150x150.jpg 150w, http://staging.carpetcourt.nz/wp-content/uploads/2016/03/Luxaflex-Countrywoods-Shutters-9-LR-300x300.jpg 300w" sizes="(max-width: 600px) 100vw, 600px">                                                <div class="isotop-content" data-backgroundcolor-hover="">
		                <div class="isocontent-wrap isocontent-wrap-default">
		                    <div class="isocontent-span">
		                                                                    </div>
		                </div>
		                <div class="idea-hover-overlay">
		                                    </div>
		                </div>
		            </a>        </div>
		            <div class="bathrooms  isotope-item isotope-2xh isotope-2xw   cc-gallery-zoom " style="position: absolute; left: 760px; top: 200px;">
		            <a href="http://staging.carpetcourt.nz/popup-page-demo-2/?iframe=true&amp;width=100%&amp;height=100%" rel="prettyPhoto">                                    <img width="600" height="600" src="http://staging.carpetcourt.nz/wp-content/uploads/2016/03/Luxaflex-Countrywoods-Shutters-9-LR-600x600.jpg" class="attachment-cc_gal_image size-cc_gal_image wp-post-image" alt="Luxaflex Countrywoods Shutters  (9) LR" srcset="http://staging.carpetcourt.nz/wp-content/uploads/2016/03/Luxaflex-Countrywoods-Shutters-9-LR-600x600.jpg 600w, http://staging.carpetcourt.nz/wp-content/uploads/2016/03/Luxaflex-Countrywoods-Shutters-9-LR-150x150.jpg 150w, http://staging.carpetcourt.nz/wp-content/uploads/2016/03/Luxaflex-Countrywoods-Shutters-9-LR-300x300.jpg 300w" sizes="(max-width: 600px) 100vw, 600px">                                                <div class="isotop-content" data-backgroundcolor-hover="">
		                <div class="isocontent-wrap isocontent-wrap-default">
		                    <div class="isocontent-span">
		                                                                    </div>
		                </div>
		                <div class="idea-hover-overlay">
		                                    </div>
		                </div>
		            </a>        </div>
		            <div class="living  isotope-item isotope-1xh isotope-1xw  cc-gallery-zoom " style="position: absolute; left: 0px; top: 400px;">
		            <a href="http://staging.carpetcourt.nz/wp-content/uploads/2016/03/PRIMETEX-Hometown-White-1695-3-LR.jpg" rel="prettyPhoto">                                    <img width="567" height="600" src="http://staging.carpetcourt.nz/wp-content/uploads/2016/03/PRIMETEX-Hometown-White-1695-3-LR-567x600.jpg" class="attachment-cc_gal_image size-cc_gal_image wp-post-image" alt="PRIMETEX Hometown White 1695 (3) LR">                                                <div class="isotop-content" data-backgroundcolor-hover="">
		                <div class="isocontent-wrap isocontent-wrap-default">
		                    <div class="isocontent-span">
		                                                                    </div>
		                </div>
		                <div class="idea-hover-overlay">
		                                    </div>
		                </div>
		            </a>        </div>
		                </div>
		            </div></div></div></div>
				</div>
				<div class="space" style="height:50px;"></div>
		    </section>
		    <!-- GALLERY STARTS -->


		    <!-- TEAM STARTS -->
			<section id="team">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_100 vc_sep_border_width_3 vc_sep_pos_align_center vc_sep_color_grey wow fadeInUp vc_separator-has-text animated" style="visibility: visible; animation-name: fadeInUp;">
								<span class="vc_sep_holder vc_sep_holder_l">
									<span class="vc_sep_line"></span>
								</span>
								<h4>Meet the team</h4>
								<span class="vc_sep_holder vc_sep_holder_r">
									<span class="vc_sep_line"></span>
								</span>
							</div>
						</div>

						<div class="col-md-12  wow fadeInUp">
							<div class="bxslider">
								<div class="slides">
									<div class="content">
										<span class="user-img">
											<img src="<?php echo get_template_directory_uri();?>/assets/images/jane.png" alt="jane">
										</span>
										<div class="user-detail">
											<h3 class="name">Jane Doe</h3>
											<h4 class="designation">Big Boss</h4>
										</div>
									</div>

									<div class="hover-content">
										<div class="user-detail">
											<h3 class="name">Jane Doe</h3>
											<h4 class="designation">Big Boss</h4>
										</div>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed impedit earum debitis reprehenderit molestiae tenetur dicta enim hic</p>
									</div>
								</div>

								<div class="slides">
									<div class="content">
										<span class="user-img">
											<img src="<?php echo get_template_directory_uri();?>/assets/images/woman.png" alt="woman">
										</span>
										<div class="user-detail">
											<h3 class="name">Jane dane</h3>
											<h4 class="designation">Medium Boss</h4>
										</div>
									</div>

									<div class="hover-content">
										<div class="user-detail">
											<h3 class="name">Jane dane</h3>
											<h4 class="designation">Medium Boss</h4>
										</div>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed impedit earum debitis reprehenderit molestiae tenetur dicta enim hic</p>
									</div>
								</div>

								<div class="slides">
									<div class="content">
										<span class="user-img">
											<img src="<?php echo get_template_directory_uri();?>/assets/images/man.png" alt="man">
										</span>
										<div class="user-detail">
											<h3 class="name">James Bond</h3>
											<h4 class="designation">Small Boss</h4>
										</div>
									</div>

									<div class="hover-content">
										<div class="user-detail">
											<h3 class="name">James Bond</h3>
											<h4 class="designation">Small Boss</h4>
										</div>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed impedit earum debitis reprehenderit molestiae tenetur dicta enim hic</p>
									</div>
								</div>

								<div class="slides">
									<div class="content">
										<span class="user-img">
											<img src="<?php echo get_template_directory_uri();?>/assets/images/man.png" alt="man">
										</span>
										<div class="user-detail">
											<h3 class="name">James Bond</h3>
											<h4 class="designation">Small Boss</h4>
										</div>
									</div>

									<div class="hover-content">
										<div class="user-detail">
											<h3 class="name">James Bond</h3>
											<h4 class="designation">Small Boss</h4>
										</div>
										<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sed impedit earum debitis reprehenderit molestiae tenetur dicta enim hic</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- TEAM ENDS -->


			<!-- CONTACT STARTS -->
			<section id="contact">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_100 vc_sep_border_width_3 vc_sep_pos_align_center vc_sep_color_grey wow fadeInUp vc_separator-has-text animated" style="visibility: visible; animation-name: fadeInUp;">
								<span class="vc_sep_holder vc_sep_holder_l">
									<span class="vc_sep_line"></span>
								</span>
								<h4>Contact & Location</h4>
								<span class="vc_sep_holder vc_sep_holder_r">
									<span class="vc_sep_line"></span>
								</span>
							</div>
						</div>

						<div class="col-md-12  wow fadeInUp">
							<div class="contact-details">
								<a href="tel:033792500" class="call">
									<i class="fa fa-phone"></i>03 379 2500
								</a>

								<a href="mailto:moorhouse@carpetcourt.co.nz" class="mail">
									<i class="fa fa-phone"></i>moorhouse@carpetcourt.co.nz
								</a>


								<a href="#">
									<i class="fa fa-envelope"></i>460 Moorhouse Ave Christchurch
								</a>

							</div>
						</div>


						<div class="col-md-3  wow fadeInUp">
							<div class="contact-hours">
								<i class="fa fa-clock-o"></i>
								<div>
									<label for="">Mon - Fri:</label>
									<span>8:30am - 5:30pm</span>

									<label for="">Sat:</label>
									<span>8:30am - 5:30pm</span>

									<label for="">Sun:</label>
									<span>Closed</span>
								</div>
							</div>
						</div>

						<div class="col-md-9  wow fadeInUp">
							<div class="contact-form">
								<form action="" class="form-horizontal">
									<input type="text" placeholder="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false">

									<input type="text" placeholder="your-emal" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false">

									<textarea name="your-message" placeholder="your-mesage" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false"></textarea>

									<input type="submit" value="Submit" class="wpcf7-form-control wpcf7-submit">
								</form>
							</div>
						</div>
					</div>
				</div>

				<div class="space" style="height:50px;"></div>
			</section>
			<!-- CONTACT ENDS -->


		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
